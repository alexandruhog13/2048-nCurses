#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ncurses.h>
#include <unistd.h>
#include <sys/select.h>
#include <math.h>

//pt ceas
#define S_TO_WAIT 		1
#define MILIS_TO_WAIT		0
#define KEYBOARD		0
#define SELECT_EVENT		1
#define SELECT_NO_EVENT		0

//pt automove
#define time_max 6

//pt joc
#define inaltime 31
#define lungime 78

#define cul_meniu 11
#define cul_joc 12
#define cul_alb 13
#define cul_scoreboard 14

#define cul_0 20
#define cul_2 21
#define cul_4 22
#define cul_8 23
#define cul_16 24
#define cul_32 25
#define cul_64 26
#define cul_128 27
#define cul_256 28
#define cul_512 29
#define cul_1024 30
#define cul_2048 31

//pt meniu
#define height 30
#define width 50

int new_game, opt_resume, opt_save, opt_load;

//sir de optiuni pt meniu
char *alegeri[] = {

	"New game",
	"Resume",
	"Save game",
	"Load game",
	"Colors",
	"Quit"
};

//sir de optiuni pt culori
char *culs[] = {

	"Original",
	"Green",
	"Blue",
	"Purple",
};

char * timestr (struct tm t) //functie care imi returneaza timpul
{
	char *timp;
	char aux[20];
	sprintf(aux, "%02d:%02d:%02d", t.tm_hour, t.tm_min, t.tm_sec);
	timp = strdup(aux);
	return timp;
}
char * datestr (struct tm t) //functie care imi returneaza data
{
	char *timp;
	char aux[20];
	if (t.tm_mon < 10)
	{	if (t.tm_mday < 10)
			sprintf(aux, "0%d/0%d/%d", t.tm_mday, t.tm_mon + 1, t.tm_year % 100 + 2000);
		else
			sprintf(aux, "%d/0%d/%d", t.tm_mday, t.tm_mon + 1, t.tm_year % 100 + 2000);
	}
	else

		if (t.tm_mday < 10)
			sprintf(aux, "0%d/%d/%d", t.tm_mday, t.tm_mon + 1, t.tm_year % 100 + 2000);
		else
			sprintf(aux, "%d/%d/%d", t.tm_mday, t.tm_mon + 1, t.tm_year % 100 + 2000);

	timp = strdup(aux);
	return timp;
}

void ceas(WINDOW *joc, int scor)//functie care imi deseneaza scoreboard-ul
{
	struct tm *t;
	time_t tim;
	tim = time(NULL);
	t = localtime(&tim);
	char *timp, *date;
	timp = timestr(*t);
	date = datestr(*t);
	wattron(joc, COLOR_PAIR(23) | A_BOLD);
	mvwprintw(joc, 27, 2, "Current time: %s", timp);
	mvwprintw(joc, 29, 2, "Current date: %s", date);
	mvwprintw(joc, 2, 54, "Current score: %d", scor);
	mvwprintw(joc, 8, 55, "Use arrows or");
	mvwprintw(joc, 9, 57, "w,a,s,d to");
	mvwprintw(joc, 10, 59, "move");
	mvwprintw(joc, 15, 55, "x - auto move");
	mvwprintw(joc, 16, 55, "u - undo");
	mvwprintw(joc, 17, 55, "q - main menu");
	mvwprintw(joc, 29, 35, "<2048 made by Hogea Alexandru Ionut>");
	wattroff(joc, COLOR_PAIR(23) | A_BOLD);
	wrefresh(joc);
}


int verifica(int a[6][6]) //returneaza cate pozitii libere mai am -0 in matricea mea-
{
	int i, j, cod = 0;
	for (i = 1; i <= 4; i++)
		for (j = 1; j <= 4; j++)
			if (a[i][j] == 0)
				cod++;
	return cod;
}

int verifica_2(int a[6][6]) //returneaza 1 daca se mai poate efecta vreo mutare cand nu mai am niciun 0 - cazul in care am doua numere de acelasi fel unul langa altul, chiar daca nu mai am 0-
{
	int cod = 0;
	int i, j;
	for (j = 1; j <= 4 && cod == 0; j++)
		for (i = 2; i <= 4 && cod == 0; i++)
			if (a[i][j] == a[i - 1][j])
				cod = 1;
	for (i = 1; i <= 4 && cod == 0; i++)
		for (j = 2; j <= 4 && cod == 0; j++)
			if (a[i][j] == a[i][j - 1])
				cod = 1;

	return cod;
}

int verifica_3(int a[6][6], int k) // returneaza 1 daca se poate efectua mutarea in directia data -daca nu ma lovesc de margini-
{
	int i, j, cod = 0;
	switch (k)
	{
	case 1:
		cod = 0;

		for (j = 1; j <= 4 && cod == 0; j++) {
			i = 4;
			while (a[i][j] == 0)
				i--;
			if (i < 1)
				cod = 0;
			else
				while (a[i][j] > 0 && a[i][j] != a[i - 1][j])
					i--;
			if (i >= 1)
				cod = 1;
		}
		break;

	case 2:
		cod = 0;
		for (j = 1; j <= 4 && cod == 0; j++) {
			i = 1;
			while (a[i][j] == 0)
				i++;
			if (i > 4)
				cod = 0;
			while (a[i][j] > 0 && a[i][j] != a[i + 1][j])
				i++;
			if (i <= 4)
				cod = 1;
		}
		break;

	case 4:
		cod = 0;
		for (i = 1; i <= 4 && cod == 0; i++) {
			j = 4;
			while (a[i][j] == 0)
				j--;
			if (j < 1)
				cod = 0;
			while (a[i][j] > 0 && a[i][j] != a[i][j - 1])
				j--;
			if (j >= 1)
				cod = 1;
		}
		break;

	case 3:
		cod = 0;
		for (i = 1; i <= 4 && cod == 0; i++) {
			j = 1;
			while (a[i][j] == 0)
				j++;
			if (j > 4)
				cod = 0;
			while (a[i][j] > 0 && a[i][j] != a[i][j + 1])
				j++;
			if (j <= 4)
				cod = 1;
		}
		break;



	}

	return cod;
}

int verifca_2048(int a[6][6]) // verific daca am ajuns la 2048
{
	int i, j, cod = 0;
	for (i = 1; i <= 4 && cod == 0; i++)
		for (j = 1; j <= 4 && cod == 0; j++)
			if (a[i][j] == 2048)
				cod = 1;
	return cod;
}
void add_mat_rand(int a[6][6]) //adaug un element random in matrice
{
	int linie, coloana, sansa;
	if (verifica(a) > 0)
	{
		linie = rand() % 4 + 1;
		coloana = rand() % 4 + 1;

		if (a[linie][coloana] == 0)
		{
			sansa = rand() % 10 + 1;
			if (sansa != 10)
				a[linie][coloana] = 2;
			else
				a[linie][coloana] = 4;
		}
		else
		{
			while (a[linie][coloana] != 0)
			{
				linie = rand() % 4 + 1;
				coloana = rand() % 4 + 1;
			}

			sansa = rand() % 10 + 1;
			if (sansa != 10)
				a[linie][coloana] = 2;
			else
				a[linie][coloana] = 4;
		}
	}
}
//functiile de mai jos imi returneaza ce scor obtin in urma mutailor efectuate si imi efectueaza si mutarile in directiile respective
int sus(int a[6][6]) //functie pt mutare sus 
{
	int i, j, cod1, cod2, k;
	int scor = 0;

	for (j = 1; j <= 4; j++)
		for (i = 2; i <= 4; i++)
		{
			cod1 = 0;
			cod2 = 0;

			if (a[i][j] != 0 && a[i][j] != -1)
				for (k = i - 1; k >= 0 && cod1 == 0 && cod2 == 0; k--)
					if ((a[k][j] != 0 && a[k][j] != a[i][j] && a[k][j] != -1) || (a[k][j] == -1 && i != 1))
					{
						if (k != i - 1)
						{

							a[k + 1][j] = a[i][j];
							a[i][j] = 0;

						}
						cod1 = 1;


					}

					else

						if (a[k][j] == a[i][j])
						{
							a[k][j] = 2 * a[k][j];
							a[i][j] = 0;
							cod2 = 1;
							scor = scor + a[k][j];
						}

		}
	return scor;

}
int jos(int a[6][6])//functie pt mutare jos
{
	int i, j, cod1, cod2, k;
	int scor = 0;

	for (j = 1; j <= 4; j++)
		for (i = 3; i >= 1; i--)
		{
			cod1 = 0;
			cod2 = 0;

			if (a[i][j] != 0 && a[i][j] != -1)
				for (k = i + 1; k <= 5 && cod1 == 0 && cod2 == 0; k++)
					if ((a[k][j] != 0 && a[k][j] != a[i][j] && a[k][j] != -1) || (a[k][j] == -1 && i != 4) )
					{
						if (k != i + 1)
						{

							a[k - 1][j] = a[i][j];
							a[i][j] = 0;


						}
						cod1 = 1;

					}

					else

						if (a[k][j] == a[i][j])

						{
							a[k][j] = 2 * a[k][j];
							a[i][j] = 0;
							scor = scor + a[k][j];
							cod2 = 1;
						}

		}
	return scor; 
}
int stanga(int a[6][6])//functie pt mutare stanga
{
	int i, j, cod1, cod2, k;
	int scor = 0;

	for (i = 1; i <= 4; i++)
		for (j = 2; j <= 4; j++)
		{
			cod1 = 0;
			cod2 = 0;

			if (a[i][j] != 0 && a[i][j] != -1)
				for (k = j - 1; k >= 0 && cod1 == 0 && cod2 == 0; k--)
					if ((a[i][k] != 0 && a[i][k] != a[i][j] && a[i][k] != -1) || (a[i][k] == -1 && j != 1))
					{
						if (k != j - 1)
						{

							a[i][k + 1] = a[i][j];
							a[i][j] = 0;

						}
						cod1 = 1;


					}

					else

						if (a[i][k] == a[i][j])

						{
							a[i][k] = 2 * a[i][k];
							a[i][j] = 0;
							scor = scor + a[i][k];
							cod2 = 1;
						}

		}
	return scor;
}
int dreapta(int a[6][6]) //functie pt mutare dreapta
{
	int i, j, cod1, cod2, k;
	int scor = 0;

	for (i = 1; i <= 4; i++)
		for (j = 3; j >= 1; j--)
		{
			cod1 = 0;
			cod2 = 0;

			if (a[i][j] != 0 && a[i][j] != -1)
				for (k = j + 1; k <= 5 && cod1 == 0 && cod2 == 0; k++)
					if ((a[i][k] != 0 && a[i][k] != a[i][j] && a[i][k] != -1) || ( a[i][k] == -1 && j != 4))
					{
						if (k != j + 1)
						{

							a[i][k - 1] = a[i][j];
							a[i][j] = 0;

						}

						cod1 = 1;

					}

					else

						if (a[i][k] == a[i][j])

						{
							a[i][k] = 2 * a[i][k];
							a[i][j] = 0;
							scor = a[i][k] + scor;
							cod2 = 1;
						}

		}
	return scor;

}


void subliniare (WINDOW *meniu, int alegere, int param) //functie care imi scrie meniul (principal si secundar, pt culori) si imi face selectiile pe optiuni
{

	int i;
	int y = 13, x = 21;

	wbkgd(meniu, COLOR_PAIR(11));
	wattrset(meniu, A_BOLD);

	mvwaddstr(meniu, 2, 11, "   ___    ____  __ __ ____ ");
	mvwaddstr(meniu, 3, 11, "  |__ \\  / __ \\/ // /( __ )");
	mvwaddstr(meniu, 4, 11, "   __/ // / / / // /_/ __ |");
	mvwaddstr(meniu, 5, 11, "  / __// /_/ /__  __/ /_/ / ");
	mvwaddstr(meniu, 6, 11, " /____/\\____/  /_/  \\____/  ");
	mvwaddstr(meniu, 7, 11, "                          ");
	wattron(meniu, COLOR_PAIR(1));
	mvwaddstr(meniu, 8, 1, "                                                  ");
	for (i = 0; i < 31; i++)
		mvwaddstr(meniu, i, 5, "  ");
	wattroff(meniu, COLOR_PAIR(1));
	if (param == 0)//daca se apeleaza pt meniul principal
	{

		for (i = 1; i <= 6; i++)
		{
			if (i == 2)
			{
				if (opt_resume == 0 && opt_save == 0 && opt_load == 0)
				{
					i = 5;
				}
				else if (opt_resume == 0 && opt_save == 0 && opt_load == 1)
				{
					i = 4;
				}
				else if (opt_resume == 0 && opt_save == 1)
					i = 3;
			}
			else if (i == 3)
			{
				if (opt_save == 0 && opt_load == 1)
				{
					i = 4;
				}
				else if (opt_save == 0 && opt_load == 0)
					i = 5;
			}
			else if (i == 4)
				if (opt_load == 0)
				{
					i = 5;
				}

			if (alegere == i)
			{
				if (i == 5)
				{
					mvwaddch(meniu, y, x - 2, '!');
					mvwaddch(meniu, y, x + strlen(alegeri[i - 1]) + 1, '!');
				}
				else
				{
					mvwaddch(meniu, y, x - 2, '*');
					mvwaddch(meniu, y, x + strlen(alegeri[i - 1]) + 1, '*');
				}
				wattron(meniu, A_STANDOUT);

				mvwprintw(meniu, y, x, "%s", alegeri[i - 1]);
				wattroff(meniu, A_STANDOUT);

			}
			else
			{
				mvwprintw(meniu, y, x, "%s", alegeri[i - 1]);
				mvwaddch(meniu, y, x - 2, ' ');
				mvwaddch(meniu, y, x + strlen(alegeri[i - 1]) + 1, ' ');
			}

			y += 2;
		}
	}
	else //daca se apeleaza pt submeniul de culori
	{
		for (i = 1; i <= 4; i++)
		{
			if (alegere == i)
			{
				mvwaddch(meniu, y, x - 2, '*');
				mvwaddch(meniu, y, x + strlen(culs[i - 1]) + 1, '*');

				wattron(meniu, A_STANDOUT);

				mvwprintw(meniu, y, x, "%s", culs[i - 1]);
				wattroff(meniu, A_STANDOUT);
			}
			else
			{
				mvwprintw(meniu, y, x, "%s", culs[i - 1]);
				mvwaddch(meniu, y, x - 2, ' ');
				mvwaddch(meniu, y, x + strlen(culs[i - 1]) + 1, ' ');
			}

			y += 2;
		}
	}
	box(meniu, 0, 0);
	wrefresh(meniu);
}

WINDOW *creare_meniu(int alegere)//functie care imi initializeaza fereastra de meniu
{
	WINDOW *meniu;

	int y, x;
	y = (LINES - height) / 2;
	x = (COLS - width) / 2;

	meniu = newwin(height, width, y, x);
	wattrset(meniu, A_BOLD);
	subliniare(meniu, 1, 0);

	wrefresh(meniu);
	return meniu;

}

void grid_joc(WINDOW * joc, int a[6][6], int scor, int cod_joc)//functie care imi scrie fereastra de joc 
{

	int row, col, k, i, j;

	wclear(joc);
	wbkgd(joc, COLOR_PAIR(2));
	ceas(joc, scor);
	wrefresh(joc);
	wattrset(joc, A_BOLD);

	if (cod_joc == 0) {
		wattron(joc, COLOR_PAIR(11));
		for (i = 0; i <= 25; i++)
			mvwaddstr(joc, i, 50, "  ");
		for (j = 2; j <= 50; j++)
			mvwaddch(joc, 25, j, ' ');
		wattroff(joc, COLOR_PAIR(11));

		for (row = 1; row <= 4; row++)
		{
			i = row * 6 - 6;
			for (col = 1; col <= 4; col++)
			{
				j = col * 12 - 11;
				if (a[row][col] == 0)
				{
					wattron(joc, COLOR_PAIR(10));
					for (k = 1; k <= 10; k++)
					{

						mvwaddch(joc, i + 2, j + k, ' ');
						mvwaddch(joc, i + 4, j + k, ' ');
						mvwaddch(joc, i + 3, j + k, ' ');
						mvwaddch(joc, i + 1, j + k, ' ');
						mvwaddch(joc, i + 5, j + k, ' ');
					}
					wattroff(joc, COLOR_PAIR(10));

				}
				else
				{
					wattron(joc, COLOR_PAIR(log2(a[row][col]) + 10));

					for (k = 1; k <= 10; k++)
					{
						mvwaddch(joc, i + 2, j + k, ' ');
						mvwaddch(joc, i + 4, j + k, ' ');
						mvwaddch(joc, i + 1, j + k, ' ');
						mvwaddch(joc, i + 5, j + k, ' ');
					}
					if (a[row][col] < 10)
						mvwprintw(joc, i + 3, j + 1, "    %d     ", a[row][col]);
					else if (a[row][col] > 10 && a[row][col] < 100)
						mvwprintw(joc, i + 3, j + 1, "    %d    ", a[row][col]);
					else if (a[row][col] > 100 && a[row][col] < 1000)
						mvwprintw(joc, i + 3, j + 1, "   %d    ", a[row][col]);
					else if (a[row][col] > 1000)
						mvwprintw(joc, i + 3, j + 1, "   %d   ", a[row][col]);
					wattroff(joc, COLOR_PAIR(log2(a[row][col]) + 10));
				}

			}
		}


		box(joc, 0, 0);
		wrefresh(joc);
	}
	else //daca am pierdut sau castigat, grid_joc nu imi va mai genera tabla de joc, ci un mesaj de final
	{
		wclear(joc);
		wrefresh(joc);
		wattron(joc, COLOR_PAIR(11));
		mvwprintw(joc, 6, 9, "                                                           ");
		mvwprintw(joc, 1, 1, "    ");
		mvwprintw(joc, 2, 1, "    ");

		mvwprintw(joc, 1, 70, "    ");
		mvwprintw(joc, 2, 70, "    ");

		mvwprintw(joc, 26, 1, "    ");
		mvwprintw(joc, 27, 1, "    ");

		mvwprintw(joc, 26, 70, "    ");
		mvwprintw(joc, 27, 70, "    ");
		mvwprintw(joc, 14, 9, "                                                           ");
		wattroff(joc, COLOR_PAIR(11));
		wattron(joc, COLOR_PAIR(23));
		mvwprintw(joc, 17, 20, "Your final score is: %d", scor);
		mvwprintw(joc, 20, 20, "Press q to return to Main Menu");
		wattroff(joc, COLOR_PAIR(23));

		if (cod_joc == 1)
		{
			wattron(joc, COLOR_PAIR(23));
			mvwprintw(joc, 7, 9, "__   __                                _     _ ");
			mvwprintw(joc, 8, 11, "\\ \\ / /                               | |   | |");
			mvwprintw(joc, 9, 11, " \\ V / ___   _   _   _ __  ___    ___ | | __| |");
			mvwprintw(joc, 10, 11, "  \\ / / _ \\ | | | | | '__|/ _ \\  / __|| |/ /| |");
			mvwprintw(joc, 11, 11, "  | || (_) || |_| | | |  | (_) || (__ |   < |_|");
			mvwprintw(joc, 12, 11, "  \\_/ \\___/  \\__,_| |_|   \\___/  \\___||_|\\_\\(_)");
			wattroff(joc, COLOR_PAIR(23));
		}
		else if (cod_joc == -1)
		{

			wattron(joc, COLOR_PAIR(23));
			mvwprintw(joc, 7, 9, " _____                                                  ");
			mvwprintw(joc, 8, 9, "|  __ \\                                                 ");
			mvwprintw(joc, 9, 9, "| |  \\/  __ _  _ __ ___    ___    ___ __   __ ___  _ __ ");
			mvwprintw(joc, 10, 9, "| | __  / _` || '_ ` _ \\  / _ \\  / _  \\ \\ / // _ \\| '__|");
			mvwprintw(joc, 11, 9, "| |_\\ \\| (_| || | | | | ||  __/ | (_) |\\ V /|  __/| |   ");
			mvwprintw(joc, 12, 9, " \\____/ \\__,_||_| |_| |_| \\___|  \\___/  \\_/  \\___||_|   ");
			wattroff(joc, COLOR_PAIR(23));
		}

		wrefresh(joc);
		wattroff(joc, COLOR_PAIR(23));
	}
}
void matrix_new_game(int a[6][6])//functie de initializare a matricei la inceputul jocului
{

	int linie, coloana, sansa;
	int i, j;

	for (i = 1; i <= 4; i++)
		for (j = 1; j <= 4; j++)
			a[i][j] = 0;
	//initializez matricea cu 2 valori random 
	linie = rand() % 4 + 1;
	coloana = rand() % 4 + 1;
	sansa = rand() % 10 + 1;
	if (sansa != 10)
		a[linie][coloana] = 2;
	else
		a[linie][coloana] = 4;

	linie = rand() % 4 + 1;
	coloana = rand() % 4 + 1;
	while (a[linie][coloana] != 0)
	{
		linie = rand() % 4 + 1;
		coloana = rand() % 4 + 1;
	}
	sansa = rand() % 10 + 1;
	if (sansa != 10)
		a[linie][coloana] = 2;
	else
		a[linie][coloana] = 4;

}

WINDOW *creare_joc(int a[6][6])//functie de initializare a ferestrei de joc
{
	WINDOW *joc;
	int scor = 0;
	int y, x;
	y = (LINES - inaltime) / 2;
	x = (COLS - lungime) / 2;
	joc = newwin(inaltime, lungime, y, x);
	grid_joc(joc, a, scor, 0);
	wrefresh(joc);
	return joc;
}

void destroy_win(WINDOW * local_win)//functi care imi sterge o fereastra 
{
	wborder(local_win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
	wclear(local_win);
	wrefresh(local_win);
	delwin(local_win);
}
void init_culori(int codcul) //functie care imi initializeaza culorile pe care le voi folosi
{
	if (codcul == 0)//original
	{

		init_color(cul_4, 933, 910, 667);
		init_pair(12, COLOR_BLACK, cul_4);

		init_color(cul_8, 956, 643, 377);
		init_pair(13, cul_alb, cul_8);

		init_color(cul_16, 255 * 3.92, 148 * 3.92, 3.92 * 105);
		init_pair(14, cul_alb, cul_16);

		init_color(cul_32, 255 * 3.92, 116 * 3.92, 65 * 3.92);
		init_pair(15, cul_alb, cul_32);

		init_color(cul_64, 255 * 3.92, 3.92 * 84, 25 * 3.92);
		init_pair(16, cul_alb, cul_64);

		init_color(cul_512, 255 * 3.92, 246 * 3.92, 0);
		init_pair(19, cul_alb, cul_128);

		init_color(cul_128, 255 * 3.92, 192 * 3.92, 5 * 3.92);
		init_pair(17, cul_alb, cul_256);

		init_color(cul_256, 247 * 3.92, 234 * 3.92, 26 * 3.92);
		init_pair(18, cul_alb, cul_512);

		init_color(cul_1024, 230 * 3.92, 226 * 3.92, 0);
		init_pair(20, cul_alb, cul_1024);

		init_color(cul_2048, 230 * 3.92, 176 * 3.92, 0);
		init_pair(21, cul_alb, cul_2048);
	}
	else if (codcul == 1)//verde
	{
		init_color(cul_4, 124 * 3.92, 255 * 3.92, 80 * 3.92);
		init_pair(12, COLOR_BLACK, cul_4);

		init_color(cul_8, 51 * 3.92, 200 * 3.92, 0);
		init_pair(13, cul_alb, cul_8);

		init_color(cul_16, 47 * 3.92, 185 * 3.92, 0);
		init_pair(14, cul_alb, cul_16);

		init_color(cul_32, 35 * 3.92, 140 * 3.92, 0 * 3.92);
		init_pair(15, cul_alb, cul_32);

		init_color(cul_64, 0 * 3.92, 3.92 * 105, 0 * 3.92);
		init_pair(16, cul_alb, cul_64);

		init_color(cul_512, 0 * 3.92, 165 * 3.92, 46 * 3.92);
		init_pair(19, cul_alb, cul_128);

		init_color(cul_128, 0 * 3.92, 90 * 3.92, 0 * 3.92);
		init_pair(17, cul_alb, cul_256);

		init_color(cul_256, 56 * 3.92, 110 * 3.92, 0 * 3.92);
		init_pair(18, cul_alb, cul_512);

		init_color(cul_1024, 0 * 3.92, 90 * 3.92, 26 * 3.92);
		init_pair(20, cul_alb, cul_1024);

		init_color(cul_2048, 0 * 3.92, 155 * 3.92, 44 * 3.92);
		init_pair(21, cul_alb, cul_2048);

	}
	else if (codcul == 2)//albastru
	{
		init_color(cul_4, 90 * 3.92, 255 * 3.92, 209 * 3.92);
		init_pair(12, COLOR_BLACK, cul_4);

		init_color(cul_8, 0 * 3.92, 255 * 3.92, 163 * 3.92);
		init_pair(13, cul_alb, cul_8);

		init_color(cul_16, 0 * 3.92, 180 * 3.92, 130 * 3.92);
		init_pair(14, cul_alb, cul_16);

		init_color(cul_32, 0 * 3.92, 135 * 3.92, 185 * 3.92);
		init_pair(15, cul_alb, cul_32);

		init_color(cul_64, 0 * 3.92, 3.92 * 113, 155 * 3.92);
		init_pair(16, cul_alb, cul_64);

		init_color(cul_512, 0 * 3.92, 29 * 3.92, 100 * 3.92);
		init_pair(19, cul_alb, cul_128);

		init_color(cul_128, 0 * 3.92, 95 * 3.92, 135 * 3.92);
		init_pair(17, cul_alb, cul_256);

		init_color(cul_256, 0 * 3.92, 71 * 3.92, 145 * 3.92);
		init_pair(18, cul_alb, cul_512);

		init_color(cul_1024, 0 * 3.92, 90 * 3.92, 26 * 3.92);
		init_pair(20, cul_alb, cul_1024);

		init_color(cul_2048, 41 * 3.92, 0 * 3.92, 140 * 3.92);
		init_pair(21, cul_alb, cul_2048);

	}
	else if (codcul == 3)//purpuriu
	{
		init_color(cul_4, 255 * 3.92, 140 * 3.92, 199 * 3.92);
		init_pair(12, COLOR_BLACK, cul_4);

		init_color(cul_8, 255 * 3.92, 50 * 3.92, 255 * 3.92);
		init_pair(13, cul_alb, cul_8);

		init_color(cul_16, 255 * 3.92, 5 * 3.92, 255 * 3.92);
		init_pair(14, cul_alb, cul_16);

		init_color(cul_32, 167 * 3.92, 0 * 3.92, 225 * 3.92);
		init_pair(15, cul_alb, cul_32);

		init_color(cul_64, 134 * 3.92, 3.92 * 0, 180 * 3.92);
		init_pair(16, cul_alb, cul_64);

		init_color(cul_512, 158 * 3.92, 31 * 3.92, 124 * 3.92);
		init_pair(19, cul_alb, cul_128);

		init_color(cul_128, 190 * 3.92, 0 * 3.92, 139 * 3.92);
		init_pair(17, cul_alb, cul_256);

		init_color(cul_256, 145 * 3.92, 0 * 3.92, 106 * 3.92);
		init_pair(18, cul_alb, cul_512);

		init_color(cul_1024, 225 * 3.92, 0 * 3.92, 58 * 3.92);
		init_pair(20, cul_alb, cul_1024);

		init_color(cul_2048, 150 * 3.92, 0 * 3.92, 43 * 3.92);
		init_pair(21, cul_alb, cul_2048);
	}

}
void init_col()//functie care imi apeleaza functiile din ncurses si imi mai initializeaza niste culori, altele decat cele pt casutele cu numere 
{

	initscr();
	clear();
	noecho();
	cbreak();
	start_color();
	curs_set(FALSE);

	init_color(cul_alb, 1000, 1000, 1000);

	init_color(cul_joc, 139 * 3.92, 131 * 3.92, 120 * 3.92);

	init_color(cul_2, 1000, 1000, 960);
	init_pair(11, COLOR_BLACK, cul_2);

	init_color(cul_0, 205 * 3.92, 192 * 3.92, 176 * 3.92);
	init_pair(10, cul_0, cul_0);

	init_pair(1, COLOR_BLACK, COLOR_BLACK);

	init_pair(2, cul_joc, cul_joc);


	init_pair(23, cul_2, cul_joc);

}

void anulare_fereastra(WINDOW * fereastra)//functie care imi "ascunde" o fereastra
{
	wclear(fereastra);
	wbkgd(fereastra, COLOR_PAIR(1));
	wrefresh(fereastra);
}

void init_matr(int a[6][6], int b[6][6]) //functie care imi copiaza o matrice in alta matrice
{
	int i, j;
	for (i = 0; i < 6; i++)
		for (j = 0; j < 6; j++)
			b[i][j] = a[i][j];
}
int simulare(int a[6][6], int k) //functie care imi simuleaza mutarile si imi returneaza numarul de 0-uri facute
{
	int nr = 0, b[6][6];

	init_matr(a, b);
	switch (k)
	{
	case 1:
		sus(b);
		nr = verifica(b);
		break;

	case 2:
		jos(b);
		nr = verifica(b);
		break;

	case 3:
		dreapta(b);
		nr = verifica(b);
		break;

	case 4:
		stanga(b);
		nr = verifica(b);
		break;
	}
	return nr;
}
void citestefisier(int a[6][6], int *scor)//functie care imi citeste din fisier matricea si scorul, pt load game
{
	FILE *f;
	f = fopen("date.in", "rb");
	int i, j;
	for (i = 1; i <= 7; i++)
		if (i < 7)
			for (j = 1; j < 7; j++)
				fread(&a[i - 1][j - 1], sizeof(a[i - 1][j - 1]), 1, f);
		else
			fread(scor, sizeof(scor), 1, f);
	fclose(f);
}
void afiseazafisier(int a[6][6], int *scor)//functie care imi scrie in fisier matricea si scorul, pt save game
{
	FILE *g;
	g = fopen("date.in", "wb");
	int i, j;
	for (i = 1; i <= 7; i++)
		if (i < 7)
			for (j = 1; j < 7; j++)
				fwrite(&a[i - 1][j - 1], sizeof(a[i - 1][j - 1]), 1, g);
		else
			fwrite(scor, sizeof(scor), 1, g);

	fclose(g);
}
int main()
{
	WINDOW *meniu, *joc;
	int c;
	int q;
	int alegere = 1, selectie = 0, i, autom = 0, mutari, sim, indice, simmax, indsim, scor = 0, undscr = 0;
	int pauza = 0, codcul = 0, colalegere, colselectie, colc;
	int nfds, sel, cod_joc;
	int a[6][6], b[6][6];
	fd_set read_descriptors;
	struct timeval timeout;
	nfds = 1;
	opt_load = 1;
	opt_save = 0;
	FD_ZERO(&read_descriptors);
	FD_SET(KEYBOARD, &read_descriptors);
	timeout.tv_sec = S_TO_WAIT;
	timeout.tv_usec = MILIS_TO_WAIT;
	init_col();
	joc = creare_joc(a);
	anulare_fereastra(joc);
	meniu = creare_meniu(alegere);
	subliniare(meniu, 1, 0);
	keypad(meniu, TRUE);
	keypad(joc, TRUE);
	srand(time(NULL));

	for (i = 0; i <= 5; i++)
		a[i][0] = a[i][5] = a[0][i] = a[5][i] = -1; //bordez matricea cu -1 pe margini pt ca asa imi este mai usor sa fac anumite verificari
	undscr = scor;
	while (selectie != 6) //cat timp nu se apasa Quit
	{
		subliniare(meniu, alegere, 0);
		selectie = 0;
		sel = select(nfds, &read_descriptors, NULL, NULL, &timeout);
		if (sel == 1)//daca in interval de o secunda apas ceva, imi preia tasta apasata
		{
			c = wgetch(meniu);
		}

		else
		{
			c = 0;//altfel, mi-o face 0
		}

		FD_SET(KEYBOARD, &read_descriptors);
		timeout.tv_sec = S_TO_WAIT;
		timeout.tv_usec = MILIS_TO_WAIT;

		switch (c)
		{
		case KEY_UP:
			if (alegere == 1)
				alegere = 1;
			else//verificarile de mai jos sunt facute pt a sublinia optiunile atunci cand nu sunt toate activate, cum ar fi resume, save game sau load game
			{
				alegere--;
				if (alegere == 4 && opt_save == 0 && opt_resume == 0 && opt_load == 0)
					alegere = 1;
				else if (alegere == 3 && opt_resume == 0 && opt_save==0)
					alegere = 1;
				else if (alegere ==3 && opt_save==0 && opt_resume==1)
					alegere=2;
			}
			break;

		case KEY_DOWN:
			if (alegere < 6)
			{//la fel ca mai sus, verific sa vad daca subliniez o optiune invalida, si daca fac asta, schimb valoarea variabilei care imi retine alegerea
				alegere++;
				if (alegere == 2)
				{	if (opt_resume == 0 && opt_save == 0 && opt_load == 0)
						alegere = 5;
					else if (opt_resume == 0 && opt_save == 1 && opt_load == 1)
						alegere = 3;
					else if (opt_resume == 0 && opt_load == 1)
						alegere = 4;
				}
				else if (alegere == 3)
				{	if (opt_save == 0 )
						alegere = 4;
				}
				else if (alegere == 4)
					if (opt_load == 0)
						alegere = 5;
			}
			else
				alegere = 6;

			break;

		case 10:
			selectie = alegere;
			break;
		}

		if (selectie == 2)//acelasi principiu de verificare, doar ca pentru selectie
		{	if (opt_resume == 0 && opt_save == 0 && opt_load == 0)
				selectie = 5;
			else if (opt_resume == 0)
				selectie = 3;
		}
		else if (selectie == 3)
		{	if (opt_save == 0)
				selectie = 5;
		}
		else if (selectie == 4)
		{	if (opt_load == 0)
				selectie = 5;
		}
		if (selectie == 3)//daca am apasat save
		{
			afiseazafisier(a, &scor);//incarc matricea si scorul in fisier
		}
		if (selectie == 4)//daca am apasat load
		{
			citestefisier(a, &scor);//incarc matricea si scorul din fisier
			opt_resume = 1;//activez optiunea pt resume 
			init_culori(codcul);//initializez paleta de culori in uz
			wclear(meniu);
			subliniare(meniu, 4, 1);
		}
		if (selectie == 1) //daca e new game
		{
			init_culori(codcul);
			undscr = scor = 0;
			cod_joc = 0;
			opt_save = 1;//activez optiunea de save 
			pauza = 0;
			matrix_new_game(a);
			anulare_fereastra(meniu);//ascund meniul

			if (opt_resume == 0)
				opt_resume = 1;//daca nu era activata, activez optiunea de resume

			grid_joc(joc, a, scor, cod_joc);
			while (pauza == 0) // nu am apasat q in joc
			{
				sel = select(nfds, &read_descriptors, NULL, NULL, &timeout);

				if (sel == 1)
				{
					q = wgetch(joc);//daca am apasat ceva in interval de secunda, preia tasta
					autom = 0;//si imi reseteaza timer-ul pentru mutarea automata
				}
				else//daca nu am apasat nimic in interval de o secunda
				{
					undscr = scor;//retin scorul
					ceas(joc, scor);//actualizez tabla (ceasul, in principal)
					q = 0;//tasta apasata este 0 (implicit)
					autom++;//incrementez timer-ul pt mutarea automata
				}

				FD_SET(KEYBOARD, &read_descriptors);
				timeout.tv_sec = S_TO_WAIT;
				timeout.tv_usec = MILIS_TO_WAIT;

				if (autom == time_max)//daca s-a ajuns la timpul maxim alocat mutarii automate, execut mutarea automata
				{
					autom = 0;
					simmax = 0;
					for (indice = 1; indice <= 4; indice++)
					{//pt fiecare directie, verific daca pot executa simularea de mutare, si daca pot, retin numarul de 0-uri returnate
						sim = 0;
						mutari = 0;
						if (verifica_3(a, indice) == 1)
						{	if ( verifica(a) > 0)
								mutari = 1;
							else if (verifica_2(a) == 1)
								mutari = 1;
						}
						if (mutari == 1)
							sim = simulare(a, indice);
						if (sim > simmax)
						{
							simmax = sim;
							indsim = indice;//retin indicele corespunzator directiei care mi-a eliberat cele mai multe 0-uri
						}
					}

					switch (indsim)
					{//verific unde este optim si efectuez mutarea si retin scorul
					case 1://daca sus este optim
						init_matr(a, b);
						undscr = scor;
						scor = scor + sus(a);
						add_mat_rand(a);
						break;

					case 2://daca jos este optim
						init_matr(a, b);
						undscr = scor;
						scor = scor + jos(a);
						add_mat_rand(a);
						break;

					case 3://daca la dreapta este optim
						init_matr(a, b);
						undscr = scor;
						scor = scor + dreapta(a);
						add_mat_rand(a);
						break;

					case 4://daca la stanga este optim
						init_matr(a, b);
						undscr = scor;
						scor = scor + stanga(a);
						add_mat_rand(a);
						break;
					}

				}
				switch (q)//miscarile propriu zise, din taste wasd sau sus-jos-dreapta-stanga
				{//pt fiecare directie, verific daca se poate efectua mutarea, si daca da, o efectuez si actualizez scorul
				case 'w' :
					mutari = 0;
					if (verifica_3(a, 1) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{
						init_matr(a, b);
						undscr = scor;
						scor = scor + sus(a);
						add_mat_rand(a);
					}
					break;

				case 's' :
					mutari = 0;
					if (verifica_3(a, 2) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{
						init_matr(a, b);
						undscr = scor;
						scor = scor + jos(a);
						add_mat_rand(a);
					}
					break;

				case 'd' :
					mutari = 0;
					if (verifica_3(a, 3) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{
						init_matr(a, b);
						undscr = scor;
						scor = scor + dreapta(a);
						add_mat_rand(a);
					}


					break;

				case 'a':
					mutari = 0;
					if (verifica_3(a, 4) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{

						init_matr(a, b);
						undscr = scor;
						scor = scor + stanga(a);
						add_mat_rand(a);
					}

					break;

				case  KEY_UP:
					mutari = 0;
					if (verifica_3(a, 1) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{

						init_matr(a, b);
						undscr = scor;
						scor = scor + sus(a);
						add_mat_rand(a);
					}
					break;

				case  KEY_DOWN:
					mutari = 0;
					if (verifica_3(a, 2) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{

						init_matr(a, b);
						undscr = scor;
						scor = scor + jos(a);
						add_mat_rand(a);
					}
					break;

				case  KEY_RIGHT:
					mutari = 0;
					if (verifica_3(a, 3) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{

						init_matr(a, b);
						undscr = scor;
						scor = scor + dreapta(a);
						add_mat_rand(a);
					}
					break;

				case  KEY_LEFT:
					mutari = 0;
					if (verifica_3(a, 4) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{

						init_matr(a, b);
						undscr = scor;
						scor = scor + stanga(a);
						add_mat_rand(a);
					}
					break;

				case 'u'://undo
					init_matr(b, a);//imi preiau matricea principala din cea salvata
					scor = undscr;//si scorul la fel
					break;

				case 'q' ://ies din while
					pauza = 1;
					break;

				case 'x'://miscarea automata efectuata de utilizator, pe acelasi principiu ca mai sus
					autom = 0;
					simmax = 0;
					for (indice = 1; indice <= 4; indice++)
					{
						sim = 0;
						mutari = 0;
						if (verifica_3(a, indice) == 1)
						{	if ( verifica(a) > 0)
								mutari = 1;
							else if (verifica_2(a) == 1)
								mutari = 1;
						}

						if (mutari == 1)
							sim = simulare(a, indice);
						if (sim > simmax)
						{
							simmax = sim;
							indsim = indice;
						}
					}

					switch (indsim)
					{
					case 1:
						init_matr(a, b);
						undscr = scor;
						scor = scor + sus(a);
						add_mat_rand(a);
						break;

					case 2:
						init_matr(a, b);
						undscr = scor;
						scor = scor + jos(a);
						add_mat_rand(a);
						break;

					case 3:
						init_matr(a, b);
						undscr = scor;
						scor = scor + dreapta(a);
						add_mat_rand(a);
						break;

					case 4:
						init_matr(a, b);
						undscr = scor;
						scor = scor + stanga(a);
						add_mat_rand(a);
						break;
					}

					break; //break de la x - mutare automata


				}//sfarsit switch q

				if (verifca_2048(a) == 1)//verific daca am obtinut 2048
				{
					cod_joc = 1;
				}

				if (verifica(a) == 0 )//verific daca am pierdut
					if (verifica_2(a) == 0)
					{
						//jocc = 0;
						cod_joc = -1;
					}

				grid_joc(joc, a, scor, cod_joc);

			}//cat timp nu am apasat q sau nu am pierdut jocul

			subliniare(meniu, alegere, 0);
			anulare_fereastra(joc);


		}
		if (selectie == 2)//daca am apasat resume, acelasi algoritm ca mai sus 
		{
			opt_save=1;
			pauza = 0;
			anulare_fereastra(meniu);
			grid_joc(joc, a, scor, cod_joc);
			while (pauza == 0) //cat timp nu am pierdut sau nu am apasat q
			{
				sel = select(nfds, &read_descriptors, NULL, NULL, &timeout);

				if (sel == 1)
				{
					q = wgetch(joc);
					autom = 0;
				}
				else
				{
					undscr = scor;
					ceas(joc, scor);
					q = 0;
					autom++;
				}

				FD_SET(KEYBOARD, &read_descriptors);
				timeout.tv_sec = S_TO_WAIT;
				timeout.tv_usec = MILIS_TO_WAIT;

				if (autom == time_max)
				{
					autom = 0;
					simmax = 0;
					for (indice = 1; indice <= 4; indice++)
					{
						sim = 0;
						mutari = 0;
						if (verifica_3(a, indice) == 1)
						{
							if (verifica(a) > 0)
								mutari = 1;
							else if (verifica_2(a) == 1)
								mutari = 1;
						}
						if (mutari == 1)
							sim = simulare(a, indice);
						if (sim > simmax)
						{
							simmax = sim;
							indsim = indice;
						}
					}

					switch (indsim)
					{
					case 1:
						init_matr(a, b);
						undscr = scor;
						scor = scor + sus(a);
						add_mat_rand(a);
						break;

					case 2:
						init_matr(a, b);
						undscr = scor;
						scor = scor + jos(a);
						add_mat_rand(a);
						break;

					case 3:
						init_matr(a, b);
						undscr = scor;
						scor = scor + dreapta(a);
						add_mat_rand(a);
						break;

					case 4:
						init_matr(a, b);
						undscr = scor;
						scor = scor + stanga(a);
						add_mat_rand(a);
						break;
					}

				}
				switch (q)
				{
				case 'w' :
					mutari = 0;
					if (verifica_3(a, 1) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{
						init_matr(a, b);
						undscr = scor;
						scor = scor + sus(a);
						add_mat_rand(a);
					}
					break;

				case 's' :
					mutari = 0;
					if (verifica_3(a, 2) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{
						init_matr(a, b);
						undscr = scor;
						scor = scor + jos(a);
						add_mat_rand(a);
					}
					break;

				case 'd' :
					mutari = 0;
					if (verifica_3(a, 3) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{
						init_matr(a, b);
						undscr = scor;
						scor = scor + dreapta(a);
						add_mat_rand(a);
					}


					break;

				case 'a':
					mutari = 0;
					if (verifica_3(a, 4) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{

						init_matr(a, b);
						undscr = scor;
						scor = scor + stanga(a);
						add_mat_rand(a);
					}

					break;

				case  KEY_UP:
					mutari = 0;
					if (verifica_3(a, 1) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{

						init_matr(a, b);
						undscr = scor;
						scor = scor + sus(a);
						add_mat_rand(a);
					}
					break;

				case  KEY_DOWN:
					mutari = 0;
					if (verifica_3(a, 2) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{

						init_matr(a, b);
						undscr = scor;
						scor = scor + jos(a);
						add_mat_rand(a);
					}
					break;

				case  KEY_RIGHT:
					mutari = 0;
					if (verifica_3(a, 3) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{

						init_matr(a, b);
						undscr = scor;
						scor = scor + dreapta(a);
						add_mat_rand(a);
					}
					break;

				case  KEY_LEFT:
					mutari = 0;
					if (verifica_3(a, 4) == 1)
					{	if ( verifica(a) > 0)
							mutari = 1;
						else if (verifica_2(a) == 1)
							mutari = 1;
					}
					if (mutari == 1)
					{

						init_matr(a, b);
						undscr = scor;
						scor = scor + stanga(a);
						add_mat_rand(a);
					}
					break;

				case 'u':
					init_matr(b, a);
					scor = undscr;
					break;

				case 'q' :
					pauza = 1;
					break;

				case 'x':
					autom = 0;
					simmax = 0;
					for (indice = 1; indice <= 4; indice++)
					{
						sim = 0;
						mutari = 0;
						if (verifica_3(a, indice) == 1)
						{	if ( verifica(a) > 0)
								mutari = 1;
							else if (verifica_2(a) == 1)
								mutari = 1;
						}

						if (mutari == 1)
							sim = simulare(a, indice);
						if (sim > simmax)
						{
							simmax = sim;
							indsim = indice;
						}
					}

					switch (indsim)
					{
					case 1:
						init_matr(a, b);
						undscr = scor;
						scor = scor + sus(a);
						add_mat_rand(a);
						break;

					case 2:
						init_matr(a, b);
						undscr = scor;
						scor = scor + jos(a);
						add_mat_rand(a);
						break;

					case 3:
						init_matr(a, b);
						undscr = scor;
						scor = scor + dreapta(a);
						add_mat_rand(a);
						break;

					case 4:
						init_matr(a, b);
						undscr = scor;
						scor = scor + stanga(a);
						add_mat_rand(a);
						break;
					}

					break; //break de la x - mutare automata

				}//sfarsit switch q

				if (verifca_2048(a) == 1)
				{
					cod_joc = 1;
				}
				else if (verifica(a) == 0 )
					if (verifica_2(a) == 0)
					{
						//jocc = 0;
						cod_joc = -1;
					}

				grid_joc(joc, a, scor, cod_joc);
			}//cat timp nu am apasat q sau nu am pierdut jocul

			subliniare(meniu, alegere, 0);
			anulare_fereastra(joc);
		}
		//sfarsit if selectie ==2
		if (selectie == 5)//daca am ales optiunea pt paleta de culori
		{

			wclear(meniu);
			wrefresh(meniu);

			colalegere = 1;
			subliniare(meniu, colalegere, 1);//rescriu submeniul pt culori
			colselectie = 0;
			while (colselectie == 0)//cat timp nu s-a facut nicio selectie
			{

				colc = wgetch(meniu);
				switch (colc)//algoritmul clasic de meniu pe care l-am folosit si mai sus
				{
				case KEY_UP:
					if (colalegere == 1)
						colalegere = 1;
					else
						colalegere--;
					break;
				case KEY_DOWN:
					if (colalegere < 4)
						colalegere++;
					else
						colalegere = 4;
					break;

				case 10:
					colselectie = colalegere;
					break;
				}
				subliniare(meniu, colalegere, 1);
			}

			codcul = colselectie - 1;//retin codul culorii
			init_culori(codcul);//si schimb paleta de culori
			wclear(meniu);
			wrefresh(meniu);
			subliniare(meniu, alegere, 0);//si intru in meniul principal

		}
	}
	
	refresh();
	clrtoeol();
	destroy_win(joc);
	destroy_win(meniu);
	endwin();
	return 0;
}
