all: tema

tema : meniu.c
	gcc -Wall -lm -lcurses meniu.c -o tema

run: tema
	./tema

clean: 
	rm -f *.o *~ tema
